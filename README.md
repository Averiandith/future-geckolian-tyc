# future-geckolian-tyc

Build a Crypto Dashboard (Junior/Mid). Yan Choong Tan

Hosted on Heroku: https://flask-coin-gecko.herokuapp.com/home

Stack used:
Python Flask + PostgreSQL

Database tables:
1. user
2. coin
3. user_coin (pivot table)

Key Functionality:
- Working login, register, and logout page.
- Dashboard page with coins listing and the ability to obtain fresh market rates to the database.
- User can click into each coin to see latest details of the coin from API.
- User have a budget and is able to purchase coins by entering it's desired quantity.
- User can naviagate to Order History to see previous purchases.

Side Note:
- Python packages used can be found in requirements.txt.
- Python version is 3.4.9
