from pycoingecko import CoinGeckoAPI
from commerce.models import db, Coin
from datetime import datetime

cg = CoinGeckoAPI()

def getCoin(ids, vs_currency='usd', sparkline=True):
    coinData = cg.get_coins_markets(vs_currency=vs_currency, ids=ids, sparkline=sparkline)
    return coinData

def upsertDb():
    coinIds = 'bitcoin, ethereum, cardano, binancecoin, tether, ripple, dogecoin, polkadot, usd-coin, solana, uniswap, bitcoin-cash, terra-luna, binance-usd, chainlink, litecoin, internet-computer, matic-network, wrapped-bitcoin, stellar, vechain, ethereum-classic, theta-token, filecoin, tron'
    coinsData = cg.get_coins_markets(vs_currency='usd', ids= coinIds, sparkline=False)

    # Upsert: Update if coin exists, else insert
    for coin in coinsData:

        supply = coin['total_supply']
        if supply is None:
            supply = 0

        coin_to_update = Coin.query.filter_by(name=coin['name']).first()

        now = datetime.now()
        date_time = now.strftime("%m/%d/%Y %H:%M:%S")

        # Update if coin exists
        if coin_to_update is not None:
            coin_to_update.current_price = coin['current_price']
            coin_to_update.market_cap_rank = coin['market_cap_rank']
            coin_to_update.market_cap = coin['market_cap']
            coin_to_update.market_cap_change_percentage_24h = coin['market_cap_change_percentage_24h']

            coin_to_update.updated_at = date_time

        # Insert if coin does not exist
        else:
            coin_to_create = Coin(
                coin_id=coin['id'],
                name=coin['name'],
                symbol=coin['symbol'],
                image=coin['image'],
                current_price=coin['current_price'],
                market_cap_rank=coin['market_cap_rank'],
                market_cap=coin['market_cap'],
                market_cap_change_percentage_24h=coin['market_cap_change_percentage_24h'],
                created_at=date_time,
                updated_at=date_time
            )

            db.session.add(coin_to_create)
        
        db.session.commit()

