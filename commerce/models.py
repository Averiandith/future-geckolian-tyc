from sqlalchemy import Column, String, Integer
from flask_login import UserMixin
from datetime import datetime
from commerce import bcrypt
from commerce.database import db


class User(db.Model, UserMixin):
    # Columns
    id = Column(Integer(), primary_key=True)
    username = Column(String(length=30), nullable=False, unique=True)
    email_address = Column(String(length=50), nullable=False, unique=True)
    password_hash = Column(String(length=60), nullable=False)
    budget = Column(Integer(), nullable=False, default=1000000)

    # Relationship with Association
    coin = db.relationship('Association')

    @property
    def prettier_budget(self):
        if len(str(self.budget)) >= 4:
            return f"{str(self.budget)[:-3]},{str(self.budget)[-3:]}$"
        else:
            return f"{self.budget}$"

    @property
    def password(self):
        return self.password

    @password.setter
    def password(self, plain_text_password):
        self.password_hash = bcrypt.generate_password_hash(plain_text_password).decode('utf_8')

    def check_password_correction(self, attempted_password):
        return bcrypt.check_password_hash(self.password_hash, attempted_password)

    def can_purchase(self, coin_object, quantity):
        return self.budget >= (coin_object.current_price * quantity)

class Coin(db.Model):
    # Columns
    id = Column(Integer(), primary_key=True)
    coin_id = Column(String(), nullable=False, unique=True)
    name = Column(String(length=30), nullable=False, unique=True)
    symbol = Column(String(length=10), nullable=False, unique=True)
    image = Column(String(), nullable=False)
    current_price = Column(db.Float(), nullable=False)
    market_cap_rank = Column(Integer(), nullable=False)
    market_cap = Column(db.Float(), nullable=False)
    market_cap_change_percentage_24h = Column(db.Float(), nullable=False)
    created_at = Column(db.DateTime())
    updated_at = Column(db.DateTime())

    def buy(self, user, quantity):
        # Store additional information at association table
        total_price = round(self.current_price * quantity, 2)

        now = datetime.now()
        date_time = now.strftime("%m/%d/%Y %H:%M:%S")

        a = Association(quantity_owned=quantity, price_per_unit=self.current_price, total_price=total_price, date_time_purchased=date_time)
        a.coin = self
        user.coin.append(a)

        user.budget -= total_price
        db.session.commit()
        
        return total_price

class Association(db.Model):
    __tablename__ = 'user_coin'

    # Columns
    id = Column(Integer(), primary_key=True)
    user_id = Column(db.ForeignKey('user.id'))
    coin_id = Column(db.ForeignKey('coin.id'))
    quantity_owned = Column(db.Float(), nullable=False)
    price_per_unit = Column(db.Float(), nullable=False)
    total_price = Column(db.Float(), nullable=False)
    date_time_purchased = Column(db.DateTime(), nullable=False)

    # Relationship with Coin
    coin = db.relationship('Coin')