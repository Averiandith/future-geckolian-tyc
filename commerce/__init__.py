from flask import Flask
from flask_bcrypt import Bcrypt
from commerce.database import setup_db

app = Flask(__name__)
bcrypt = Bcrypt(app)
setup_db(app)

from commerce import routes