from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

def setup_db(app):
    '''
    Binds a flask application to a SQLAlchemy service
    '''
    app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://postgres:password@localhost/cg_yanchoong'
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config['SECRET_KEY'] = '78cd973926ada26158329abb'

    db.app = app
    db.init_app(app)

# def db_drop_and_create_all():
#     '''
#     Drops the database tables and starts fresh
#     Can be used to initialize a clean database
#     '''
#     engine = create_engine('postgresql://postgres:password@localhost/cg_yanchoong')

#     Association.__table__.drop(engine)
#     User.__table__.drop(engine)
#     Coin.__table__.drop(engine)

#     db.create_all()