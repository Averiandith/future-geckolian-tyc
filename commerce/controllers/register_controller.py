from flask import flash
from flask_login import login_user
from commerce.models import User, db


def register_controller(form):
    # Form validation here
    if form.validate_on_submit():
        user_to_create = User(username=form.username.data, 
            email_address = form.email_address.data,
            password = form.password1.data)
        
        # Save the form data to database
        db.session.add(user_to_create)
        db.session.commit()

        # Directly login user after account creation
        login_user(user_to_create)
        flash(f'Account created successfully! You are now logged in as {user_to_create.username}', category='success')

        return True
    
    # There are form errors flash message
    if form.errors != {}:
        for err_msg in form.errors.values():
            flash(f'There was an error with creating user {err_msg}', category='danger')
        
        return False
