from flask import flash
from flask_login import current_user
from commerce.models import Coin
from commerce.cg_api import upsertDb

def get_dashboard_data():
    upsertDb()
    coins = Coin.query.all()
    return coins

def purchase_coin(request, purchase_form):
    purchased_coin = request.form.get('purchased_coin')
    quantity = purchase_form.quantity.data
    p_coin_object = Coin.query.filter_by(name=purchased_coin).first()

    # Make sure coin exists
    if p_coin_object:
        # User has enough budget to purchase coin
        if current_user.can_purchase(p_coin_object, quantity):
            # Only enable purchase if quantity is more than 0
            if quantity > 0:
                # Purchase the coin
                total_price = p_coin_object.buy(current_user, quantity)
                flash(f"You purchased {quantity} {p_coin_object.name}(s) for ${total_price}", category='success')
            else:
                flash(f"Please enter a valid quantity", category='danger')
        
        # User has insufficient budget
        else:
            flash(f"You have insufficient funds to purchase {p_coin_object.name}", category='danger')