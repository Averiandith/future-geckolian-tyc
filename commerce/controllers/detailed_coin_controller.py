from commerce.cg_api import getCoin

def detailed_coin_controller(coin_id):
    coin = getCoin(ids=coin_id)

    coin[0]['market_cap_change_percentage_24h'] = round(coin[0]['market_cap_change_percentage_24h'], 1) if coin[0]['market_cap_change_percentage_24h'] is not None else 0
    coin[0]['current_price'] = "${:,.2f}".format(coin[0]['current_price']) if coin[0]['current_price'] is not None else 0
    coin[0]['market_cap'] = "${:,}".format(coin[0]['market_cap']) if coin[0]['market_cap'] is not None else 0
    coin[0]['fully_diluted_valuation'] = "${:,}".format(coin[0]['fully_diluted_valuation']) if coin[0]['fully_diluted_valuation'] else 0
    coin[0]['circulating_supply'] = "{:,}".format(round(coin[0]['circulating_supply'])) if coin[0]['circulating_supply'] is not None else 0
    coin[0]['total_supply'] = "{:,}".format(round(coin[0]['total_supply'])) if coin[0]['total_supply'] is not None else 0
    coin[0]['max_supply'] = "{:,}".format(round(coin[0]['max_supply'])) if coin[0]['max_supply'] is not None else 0

    return coin