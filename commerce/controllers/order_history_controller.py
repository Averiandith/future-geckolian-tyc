from commerce.models import Coin, Association

def order_history_controller(current_user):
    order_history = Association.query.filter_by(user_id=current_user.id)

    def coin_img_query(coin_id):
        return Coin.query.filter_by(id=coin_id).first().image

    def coin_name_query(coin_id):
        return Coin.query.filter_by(id=coin_id).first().name
    
    return  {'order_history': order_history, 
        'coin_img_query': coin_img_query, 
        'coin_name_query': coin_name_query
    }