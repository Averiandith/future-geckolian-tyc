from flask import flash
from flask_login import login_user, LoginManager
from commerce.models import User
from commerce import app

login_manager = LoginManager(app)
login_manager.login_view = "login_page"
login_manager.login_message_category = "info"

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

def login_controller(form):
    # Form validation here
    if form.validate_on_submit():
        # Query database to find username
        attempted_user = User.query.filter_by(username=form.username.data).first()
        
        # Login user if password matches
        if attempted_user and attempted_user.check_password_correction(attempted_password=form.password.data):
            login_user(attempted_user)
            flash(f'Success! You are logged in as {attempted_user.username}', category='success')
            return True
        
        # Error, prompt try again message
        else:
            flash('Username and password are not match! Please try again', category='danger')
            return False