from commerce import app
from flask import render_template, redirect, url_for, flash, request
from flask_login import logout_user, login_required, current_user
from commerce.forms import RegisterForm, LoginForm, PurchaseCoinForm
from commerce.controllers.dashboard_controller import get_dashboard_data, purchase_coin
from commerce.controllers.order_history_controller import order_history_controller
from commerce.controllers.detailed_coin_controller import detailed_coin_controller
from commerce.controllers.register_controller import register_controller
from commerce.controllers.login_controller import login_controller


@app.route("/")
@app.route("/home")
def home_page():
    '''
    Landing page for CG Commerce page
    '''
    return render_template('home.html')

@app.route('/dashboard', methods=['GET', 'POST'])
@login_required
def dashboard_page():
    '''
    Route to dashboard coin page list
    '''
    purchase_form = PurchaseCoinForm()

    if request.method == "GET":
        data = get_dashboard_data()
        return render_template('dashboard.html', coins=data, purchase_form=purchase_form)

    if request.method == "POST":
        purchase_coin(request, purchase_form)
        return redirect(url_for('dashboard_page'))

@app.route('/order_history')
@login_required
def order_history_page():
    '''
    Route to order history page
    '''
    data = order_history_controller(current_user)

    return render_template(
        'order_history.html', 
        order_history=data['order_history'], 
        coin_name_query=data['coin_name_query'], 
        coin_img_query=data['coin_img_query']
    )

@app.route('/detailed_coin/<coin_id>')
@login_required
def detailed_coin(coin_id: str):
    '''
    Route to detailed coin page
    '''
    coin = detailed_coin_controller(coin_id)
    return render_template('coin_details.html', coin=coin)

@app.route('/register', methods=['GET', 'POST'])
def register_page():
    '''
    Route to account creation page
    '''
    form = RegisterForm()

    if register_controller(form):
        return redirect(url_for('dashboard_page'))

    # Bring user back to register page
    return render_template('register.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login_page():
    '''
    Route to account login page
    '''
    form = LoginForm()

    if login_controller(form):
        # Redirect user to dashboard market page
        return redirect(url_for('dashboard_page'))

    # Bring user back to login page
    return render_template('login.html', form=form)

@app.route('/logout')
def logout_page():
    '''
    Route to logout page
    '''
    logout_user()
    flash("You have been logged out!", category='info')

    return redirect(url_for("home_page"))